
import { Modal, Box, Grid, Button } from "@mui/material";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function DeleteModal({ open, setOpen, post }) {
    const fetchApi = async (paramUrl, paramOptions = {}) => {
        await fetch(paramUrl, paramOptions);
    }

    const handleClose = () => setOpen(false);
    const deleteClick = () => {
        const body = {
            method: 'DELETE',
        }

        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/' + post.id, body)
            .then((data) => {
                console.log(data);
                setOpen(false);
                window.location.reload();
            })
            .catch((error) => {
                console.log(error);
            })

    }
    return (
        <Modal
            open={open}
            onClose={handleClose}
            aria-labelledby="modal-detail-title"
            aria-describedby="modal-detail-description"
        >
            <Box sx={style}>
                <h1> Confirm xóa Order</h1>
                <hr />
                <h4>Bạn có chắc chắn muốn xóa Order này không?</h4>

                <Grid textAlign={'right'} mt={5}>

                    <Button variant='contained' sx={{ m: 1 }} color='error' onClick={deleteClick}>Xác nhận</Button>
                    <Button variant='outlined' color='error' onClick={handleClose}>Hủy bỏ</Button>

                </Grid>
            </Box>
        </Modal>
    )
}

export default DeleteModal;
