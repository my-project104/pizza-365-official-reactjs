import { Modal, Box, Grid, TextField, Select, FormControl, MenuItem, InputLabel, Button, Stack } from "@mui/material";
import { useState, useEffect } from 'react';
import ModalToast from './ModalToast.js'

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 1000,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

function UpdateModal({ open, setOpen, post }) {
    const [openModalToast, setOpenModalToast] = useState(false);
    const [infoToast, setInfoToast] = useState({});

    const fetchApi = async (paramUrl, paramOptions = {}) => {
        const response = await fetch(paramUrl, paramOptions);
        const responseData = await response.json();
        return responseData;
    }


    const updateClick = () => {

        const body = {
            method: 'PUT',
            body: JSON.stringify({
                trangThai: upTrangThai
            }),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }

        fetchApi('http://42.115.221.44:8080/devcamp-pizza365/orders/' + post.id, body)
            .then((data) => {
                console.log(data);
                setOpen(false);
                setOpenModalToast(true)
                setInfoToast({ toastColor: "green", toastThongBao: "Update thành công!", toastNoiDung: "Mã đơn hàng : " + data.orderId + "  Trạng thái : " + upTrangThai })
            })
            .catch((error) => {
                console.log(error);
            })


    }


    const handleClose = () => setOpen(false);

    const handleChange = () => setOpen(false);

    const [upTrangThai, setUpTrangThai] = useState('open');


    const updateTrangThai = (e) => {
        setUpTrangThai(e.target.value)
    }

    const [dataDrink, setDataDrink] = useState([]);
    const getDataDrink = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/drinks");
        const data = await response.json();
        return data;
    }

    useEffect(() => {
        getDataDrink()
            .then((data) => {
                console.log(data);
                setDataDrink(data);
            })
            .catch((error) => {
                console.log(error);
            })

    }, [])

    return (
        <>
            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-detail-title"
                aria-describedby="modal-detail-description"
            >
                <Box sx={style}>
                    <h2>Update Trạng Thái Order</h2>
                    <hr />

                    <Grid mt={5} mb={5}>
                        <Box sx={{ minWidth: 120 }}>

                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="ID" fullWidth value={post.id} defaultValue=" " ></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Order ID" fullWidth value={post.orderId} defaultValue=" " ></TextField>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                    <Grid mt={5}>
                        <Box sx={{ minWidth: 120 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={4}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Kích cỡ Combo</InputLabel>
                                        <Select
                                            label="Kích cỡ Combo"
                                            value={post.kichCo}
                                        >
                                            <MenuItem value="S">S</MenuItem>
                                            <MenuItem value='M'>M</MenuItem>
                                            <MenuItem value='L'>L</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Đường kính Pizza" fullWidth value={post.duongKinh} defaultValue=" " ></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Salad" fullWidth value={post.salad} defaultValue=" " ></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Số lượng nước" fullWidth value={post.soLuongNuoc} defaultValue=" " ></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Sườn" fullWidth value={post.suon} defaultValue=" " ></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Thành Tiền" fullWidth value={post.thanhTien} defaultValue=" " ></TextField>
                                </Grid>

                            </Grid>

                        </Box>
                    </Grid>

                    <Grid mt={5} mb={5}>
                        <Box sx={{ minWidth: 120 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel >Loại Pizza</InputLabel>
                                        <Select
                                            label="Loại Pizza"
                                            value={post.loaiPizza}
                                        >
                                            <MenuItem value="Seafood">Seafood</MenuItem>
                                            <MenuItem value='Hawaii'>Hawaii</MenuItem>
                                            <MenuItem value='Bacon'>Bacon</MenuItem>
                                            <MenuItem value='SEAFOOD'>SEAFOOD</MenuItem>
                                            <MenuItem value='HAWAII'>HAWAII</MenuItem>
                                            <MenuItem value='Pizza Bacon'>Pizza Bacon</MenuItem>

                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel >Loại Nước Uống</InputLabel>
                                        <Select
                                            label="Loại Nước Uống"
                                            value={post.idLoaiNuocUong}
                                        >
                                            {dataDrink.map((drink, index) => (
                                                <MenuItem key={index} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                                            ))}
                                        </Select>
                                    </FormControl>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>

                    <Grid mt={5} mb={5}>
                        <Box sx={{ minWidth: 120 }}>

                            <Grid container spacing={2}>

                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Họ và Tên" fullWidth value={post.hoTen}></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Số điện thoại" fullWidth value={post.soDienThoai}></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Email" fullWidth value={post.email}></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Địa chỉ" fullWidth value={post.diaChi}></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Voucher" fullWidth value={post.idVourcher}></TextField>
                                </Grid>
                                <Grid item xs={4}>
                                    <TextField variant="outlined" label="Lời nhắn" fullWidth value={post.loiNhan}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Giảm giá" fullWidth value={post.giamGia}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <FormControl fullWidth>
                                        <InputLabel >Trạng thái</InputLabel>
                                        <Select
                                            label="Loại Pizza"
                                            defaultValue={post.trangThai}
                                            onChange={updateTrangThai}
                                        >
                                            <MenuItem value="open">Open</MenuItem>
                                            <MenuItem value='Confirmed'>Comfirmed</MenuItem>
                                            <MenuItem value='Cancel'>Cancel</MenuItem>
                                        </Select>
                                    </FormControl>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Ngày Tạo" fullWidth value={new Date(post.ngayTao)}></TextField>
                                </Grid>
                                <Grid item xs={6}>
                                    <TextField variant="outlined" label="Ngày cập nhật" fullWidth value={new Date(post.ngayCapNhat)}></TextField>
                                </Grid>
                            </Grid>
                        </Box>
                    </Grid>
                    <hr />
                    <Box sx={{ mt: 5, mx: 45 }} >
                        <Stack spacing={2} direction="row" align='center'>
                            <Button color='success' variant="contained" size="medium" onClick={() => updateClick()}>Update</Button>
                            <Button color='success' variant="outlined" size="medium" onClick={handleChange}>Cancel</Button>
                        </Stack>
                    </Box>
                </Box>


            </Modal>

            <ModalToast open={openModalToast} setOpen={setOpenModalToast} thongBao={infoToast} />
        </>
    )
}

export default UpdateModal;
