import { Button, Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Stack, TextField } from "@mui/material";
import { useEffect, useState } from "react";
import UpdateModal from "./Modal/UpdateModal";
import SelectNumberPage from "./SelectNumberPage.js";
import EditIcon from '@mui/icons-material/Edit';
import DeleteIcon from '@mui/icons-material/Delete';
import InsertModal from "./Modal/InsertModal.js";
import DeleteModal from "./Modal/DeleteModal.js";
import AddIcon from '@mui/icons-material/Add';
import SearchIcon from '@mui/icons-material/Search';


function Datatable() {
    const [posts, setPosts] = useState([]);
    const [dataFilter, setDataFilter] = useState([]);

    const [page, setPage] = useState(1);
    const [pageSearch, setPageSearch] = useState(1);
    const [disabledPa, setDisabledPa] = useState(false);

    const [noPage, setNoPage] = useState(0);
    const [openUpdateModal, setOpenUpdateModal] = useState(false);
    const [openInsertModal, setOpenInsertModal] = useState(false);
    const [openDeleteModal, setOpenDeleteModal] = useState(false);
    const [searchText, setSearchText] = useState('');

    const [show, setShow] = useState(10)

    const limit = show;

    const callbackFunction = (childData) => {
        setShow(childData)
    }


    const [selectedPost, setSelectedPost] = useState({});


    const getData = async () => {
        const response = await fetch("http://42.115.221.44:8080/devcamp-pizza365/orders");

        const data = await response.json();

        return data;
    }

    const changeHandler = (event, value) => {
        setPage(value);
        setPageSearch(value)
    }

    const addClick = () => {
        console.log("Add");
        setOpenInsertModal(true);

    }

    const getDetailPost = (row) => {
        console.log("Edit");
        console.log("ID : " + row.id);
        setSelectedPost(row);
        setOpenUpdateModal(true);
    }
    const deleteDetailPost = (post) => {
        console.log("Delete");
        console.log("ID : " + post.id);
        setSelectedPost(post);
        setOpenDeleteModal(true);
    }
    var array = posts
    const searchTextChangeHandle = (e) => {
        setSearchText(e.target.value)

    }


    const searchClick = () => {

        array = dataFilter.filter(
            item => (item.hoTen && item.hoTen.toLowerCase().includes(searchText.toLowerCase()))
                || (item.orderId && item.orderId.toLowerCase().includes(searchText.toLowerCase()))
                || (item.kichCo && item.kichCo.toLowerCase().includes(searchText.toLowerCase()))
                || (item.loaiPizza && item.loaiPizza.toLowerCase().includes(searchText.toLowerCase()))
                || (item.idLoaiNuocUong && item.idLoaiNuocUong.toLowerCase().includes(searchText.toLowerCase()))
                || (item.soDienThoai.includes(searchText))
                || (item.trangThai && item.trangThai.toLowerCase().includes(searchText.toLowerCase()))
            ,
        )
        setPosts(array);
        setDisabledPa(true)

    }



    useEffect(() => {
        getData()
            .then((data) => {
                console.log(data);
                setNoPage(Math.ceil(data.length / limit));
                setPosts(data.slice((page - 1) * limit, page * limit));
                setDataFilter(data);
                setDisabledPa(false)

            })
            .catch((error) => {
                console.log(error);
            })
    }, [page, limit])

    return (
        <Container>
            <Grid container marginTop={10}>
                <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                    <Button
                        color="success"
                        variant="contained"
                        onClick={() => addClick()}
                        sx={{ m: 1 }}
                        startIcon={<AddIcon />}
                    >Add
                    </Button>
                </Grid>

                <Grid container rowSpacing={1} columnSpacing={{ xs: 1 }} >
                    <Grid item xs={4}>
                        <SelectNumberPage parentCallback={callbackFunction} />
                    </Grid>
                    <Grid item xs={7}>
                        <TextField fullWidth label="Search" size="medium" onChange={searchTextChangeHandle}>
                        </TextField>

                    </Grid>
                    <Grid item xs={1}>

                        <Button fullWidth size="large"><SearchIcon fontSize="large" onClick={searchClick} /></Button>
                    </Grid>

                </Grid>

                <Grid item xs={12} md={12} sm={12} lg={12}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="posts table">
                            <TableHead sx={{ backgroundColor: 'gray' }}>
                                <TableRow  >
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Order ID</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Kích cỡ Combo</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Loại Pizza</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Nước Uống</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Thành Tiền</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Họ và Tên </TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Số điện thoại</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Trạng Thái</TableCell>
                                    <TableCell sx={{ color: 'white', textAlign: "left" }}>Action</TableCell>

                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {posts.map((row, index) => (
                                    <TableRow key={index} sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
                                        <TableCell component="th" scope="row">{row.orderId}</TableCell>
                                        <TableCell>{row.kichCo}</TableCell>
                                        <TableCell>{row.loaiPizza}</TableCell>
                                        <TableCell>{row.idLoaiNuocUong}</TableCell>
                                        <TableCell>{row.thanhTien}</TableCell>
                                        <TableCell>{row.hoTen}</TableCell>
                                        <TableCell>{row.soDienThoai}</TableCell>
                                        <TableCell>{row.trangThai}</TableCell>

                                        <TableCell>
                                            <Stack spacing={2} direction="row" align='center'>
                                                <Button
                                                    variant="contained"
                                                    onClick={() => getDetailPost(row)}
                                                    startIcon={<EditIcon />}
                                                ></Button>
                                                <Button
                                                    color="error"
                                                    onClick={() => deleteDetailPost(row)}
                                                    startIcon={<DeleteIcon />}
                                                    variant="contained"
                                                >
                                                </Button>
                                            </Stack>
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={12} md={12} sm={12} lg={12} marginTop={5} marginBottom={5} marginRight={50} marginLeft={50}>
                    <Pagination onChange={changeHandler} count={noPage} defaultPage={page} hidden={disabledPa} ></Pagination>
                </Grid>
            </Grid>
            <UpdateModal open={openUpdateModal} setOpen={setOpenUpdateModal} post={selectedPost} />
            <InsertModal open={openInsertModal} setOpen={setOpenInsertModal} />
            <DeleteModal open={openDeleteModal} setOpen={setOpenDeleteModal} post={selectedPost} />

        </Container>
    )
}

export default Datatable;
